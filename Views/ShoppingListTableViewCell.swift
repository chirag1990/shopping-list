//
//  ShoppingListTableViewCell.swift
//  ShoppingList
//
//  Created by Tailor, Chirag on 09/08/2018.
//  Copyright © 2018 Tailor, Chirag. All rights reserved.
//

import UIKit

class ShoppingListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCompleted: UILabel!
    @IBOutlet weak var lblName: UILabel!
}

extension ShoppingListTableViewCell
{
    // register nib
    static func registerNib(tableView:UITableView){
        tableView.register(UINib(nibName: CELL_IDENTIFIER, bundle: nil), forCellReuseIdentifier: CELL_IDENTIFIER)
    }
}
