//
//  Constants.swift
//  ShoppingList
//
//  Created by Tailor, Chirag on 09/08/2018.
//  Copyright © 2018 Tailor, Chirag. All rights reserved.
//

import Foundation

typealias CompletionHandler = (_ Success: Bool) -> ()
typealias AlertCompletionHandler = (_ okClicked:Bool) -> Void

//CoreData
let ENTITY_NAME = "ShoppingList"
let NAME_KEY = "name"
let COMPLETED_KEY = "completed"

//Update Coredata list function helper
func formatStringForPredicate(oldListName: String) -> String {
    return "name = '\(oldListName)'"
}

//TableView
let CELL_IDENTIFIER = "ShoppingListTableViewCell"
let DELETE_TITLE = "Delete"
let EDIT_TITLE = "Edit"


//AlertView
let LIST_ADDED_TITTLE = "Shopping Item"
let LIST_ADDED_MESSAGE = "Shopping item added to list"
let LIST_ADDED_FAILED = "Shopping Item"
let LIST_ADDED_FAILED_MESSAGE = "Shopping item failed to add to list, please try again later"


//Segues
let UNWIND_TO_HOME = "unwind"
let ADD_ITEM_SEGUE_IDENTIFIED = "AddNewItem"
let EDIT_ITEM_SEGUE_IDENTIFIED = "Edit List"


//Segues Passing Data Identifier
let ADD_SEGUE_DATA = "Add New Item"
