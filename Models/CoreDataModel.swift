//
//  CoreDataModel.swift
//  ShoppingList
//
//  Created by Tailor, Chirag on 09/08/2018.
//  Copyright © 2018 Tailor, Chirag. All rights reserved.
//

import UIKit
import CoreData

class CoreDataHandler : NSObject
{
    private class func getContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    //Save The item into core data
    class func saveObject(name : String, taskCompleted: Bool) -> Bool {
        let context = getContext()
        let entity = NSEntityDescription.entity(forEntityName: ENTITY_NAME, in: context)
        let manageObject = NSManagedObject(entity: entity!, insertInto: context)
        
        manageObject.setValue(name, forKey: NAME_KEY)
        manageObject.setValue(taskCompleted, forKey: COMPLETED_KEY)
        
        do {
            try context.save()
            return true
        }catch {
            return false
        }
    }
    
    //Get the items from coredata
    class func fetchObject() -> [ShoppingList]? {
        let context = getContext()
        var list:[ShoppingList]? = nil
        do {
            list = try context.fetch(ShoppingList.fetchRequest())
            return list
        }catch {
            return list
        }
    }
    
    //update the items in coredata
    class func updateObject(name: String, completed: Bool, oldListName: String) -> Bool{
        let context = getContext()
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: ENTITY_NAME)
        fetchRequest.predicate = NSPredicate(format: formatStringForPredicate(oldListName: oldListName))
        do {
            let results = try context.fetch(fetchRequest)
            if results.count == 1 {
                let objectUpdate = results[0] as! NSManagedObject
                objectUpdate.setValue(name, forKey: NAME_KEY)
                objectUpdate.setValue(completed, forKey: COMPLETED_KEY)
                
                do{
                    try context.save()
                    return true
                }catch {
                    return false
                }
            }
        } catch {
            return false
        }
        return false
    }
    
    //delete the items in coredata
    class func deleteObject(name: String) -> Bool {
        let context = getContext()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: ENTITY_NAME)
        fetchRequest.predicate = NSPredicate(format: formatStringForPredicate(oldListName: name))
        let objects = try! context.fetch(fetchRequest)
        for obj in objects {
            context.delete(obj as! NSManagedObject)
        }
        do {
            try context.save()
            return true
        } catch {
            return false
        }
    }
}
