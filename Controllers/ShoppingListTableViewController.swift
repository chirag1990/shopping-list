//
//  ShoppingListTableViewController.swift
//  ShoppingList
//
//  Created by Tailor, Chirag on 09/08/2018.
//  Copyright © 2018 Tailor, Chirag. All rights reserved.
//

import UIKit

class ShoppingListTableViewController: UITableViewController {

    @IBOutlet var tableview: UITableView!
    
    // fetching the data from coredata
    fileprivate var dataSource = CoreDataHandler.fetchObject()
    
    //public variables
    var name: String?
    var completed: Bool?
    var localCompleted:Bool?
    var indexpath:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSettings()
    }
    
    fileprivate func initialSettings()
    {
        // setting delegate and datasource for tableview
        tableview.delegate = self
        tableview.dataSource = self
        // registering tableview cell nib
        ShoppingListTableViewCell.registerNib(tableView: tableview)
        self.loadPage()
    }
    
    fileprivate func loadPage() {
        // required when unwind segue is called and when view is loaded
        self.dataSource = CoreDataHandler.fetchObject()
        self.tableview.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        // number of section (only one section as they are all shopping items)
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        //return the count of items in coredata
        return self.dataSource!.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // setting up cell and dequeue the cell for reuse
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER, for: indexPath) as! ShoppingListTableViewCell

        let itemList = dataSource![indexPath.row]
        if itemList.completed == true {
            cell.lblCompleted.text = "✔️"
            cell.lblName.text = itemList.name
        } else {
            cell.lblName.text = itemList.name
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // cell height
        return 80
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // getting the correct cell, setting up the tick feature and save the changes in coredata
        let cell = tableView.cellForRow(at: indexPath) as! ShoppingListTableViewCell
        
        if cell.lblCompleted.text == "✔️" {
            cell.lblCompleted.text = ""
            localCompleted = false
        } else {
            cell.lblCompleted.text = "✔️"
            localCompleted = true
        }
        _ = CoreDataHandler.updateObject(name: cell.lblName.text!, completed: localCompleted!, oldListName: cell.lblName.text!)
        // below line is required to deselect the selection color
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // setting up the swipe to edit / delete
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        //no code required here as the actions are getting handled in editActionsForRowAt
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        // deleteAction, Call the deleteobject function, and then reload the data
        let deleteAction  = UITableViewRowAction(style: .default, title: DELETE_TITLE) { (rowAction, indexPath) in
            _ = CoreDataHandler.deleteObject(name: self.dataSource![indexPath.row].name!)
            self.loadPage()
        }
        
        // editAction, save the name, completed and indexpath in local variable to pass to new viewcontroller
        let editAction  = UITableViewRowAction(style: .normal, title: EDIT_TITLE) { (rowAction, indexPath) in
            self.name = self.dataSource![indexPath.row].name
            self.completed = self.dataSource![indexPath.row].completed
            self.indexpath = indexPath.row
            self.performSegue(withIdentifier: EDIT_ITEM_SEGUE_IDENTIFIED, sender: nil)
        }
        
        editAction.backgroundColor = UIColor.orange
        return [deleteAction,editAction]
    }
    
    // MARK: - Segue
    @IBAction func prepareForUnwindToListView(segue: UIStoryboardSegue) {
        // Once save has been done when user is adding new item, application comes here and runs the loadPage function
        self.loadPage()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == ADD_ITEM_SEGUE_IDENTIFIED) {
            let vc = segue.destination as! AddShoppingListViewController
            vc.isAdd = ADD_SEGUE_DATA
        } else if (segue.identifier == EDIT_ITEM_SEGUE_IDENTIFIED) {
            let vc = segue.destination as! AddShoppingListViewController
            vc.name = self.name
            vc.completed = self.completed
            vc.indexpath = self.indexpath
        }
    }
    

}
