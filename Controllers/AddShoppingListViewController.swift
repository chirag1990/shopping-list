//
//  AddShoppingListViewController.swift
//  ShoppingList
//
//  Created by Chirag Tailor on 09/08/2018.
//  Copyright © 2018 Tailor, Chirag. All rights reserved.
//

import UIKit

class AddShoppingListViewController: UIViewController {

    @IBOutlet weak var shoppingItemTextField: UITextField!
    @IBOutlet weak var tickLabel: UILabel!
    @IBOutlet weak var completedLabel: UILabel!

    // public variables
    var success:Bool?
    var list:[ShoppingList]? = nil
    var isAdd: String?
    var name: String?
    var completed: Bool?
    var indexpath : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI() {
        //check if the user is adding a new item or editing an old item
        if isAdd == ADD_SEGUE_DATA  {
            tickLabel.isHidden = true
            completedLabel.isHidden = true
        } else {
            shoppingItemTextField.text = name
            if completed == true {
                tickLabel.text = "✔️"
            } else {
                tickLabel.isHidden = true
                completedLabel.isHidden = true
            }
            
        }
    }
    
    @IBAction func SaveButtonPressed(_ sender: Any) {
        // first check if its a new item add, if yes than call the saveObject function and pass the details through.
        // else we need to pass the original name as well as new name and completed check
        if isAdd == ADD_SEGUE_DATA{
            success = CoreDataHandler.saveObject(name: shoppingItemTextField.text!, taskCompleted: false)
        } else {
            list = CoreDataHandler.fetchObject()!
            if list?.count != 0 {
                let oldListName = list![indexpath!].name
                //let oldListCompleted = list![0].completed
                success = CoreDataHandler.updateObject(name: shoppingItemTextField.text!, completed: completed!, oldListName: oldListName!)
            }
        }
        
        // alertview, once user click on ok, it will unwind to list view controller
        if success! {
            self.showAlertWithOk(title: LIST_ADDED_TITTLE, message: LIST_ADDED_MESSAGE, completionHandler: {[weak self] (success) in
                self?.performSegue(withIdentifier: UNWIND_TO_HOME, sender: self)
            })
        } else {
            self.showAlert(title: LIST_ADDED_FAILED, message: LIST_ADDED_FAILED_MESSAGE)
        }
        
    }
}
